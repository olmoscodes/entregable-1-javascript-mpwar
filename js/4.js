/**
 * Modifica las funciones para implementar la funcionalidad especificada.
 * Puedes añadir funciones adicionales si lo necesitas.
 * Este archivo debe ser ejecutable (no incluyas código inválido
 * o no relacionado con el ejercicio).
 */

function validate_password(password) {
  const regex = /^(?=.\d)(?=.[A-Z][0-9][a-z])(?=.*)[-_.,|%&-zA-Z]{8,16}/
  return regex.test(password);
}
