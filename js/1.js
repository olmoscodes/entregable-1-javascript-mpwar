/**
 * Modifica las funciones para implementar la funcionalidad especificada.
 * Puedes añadir funciones adicionales si lo necesitas.
 * Este archivo debe ser ejecutable (no incluyas código inválido
 * o no relacionado con el ejercicio).
 */

function unique(array, explicit) {
  const arrayN = [];
  const object = {};

  if (explicit == true) {
    array.forEach((el) => {
      if (!(el in object)) {
        object[el] = true;
        arrayN.push(el);
      }
    });
  } else {
    array.forEach((el) => {
      if (!(el.toUpperCase() in object)) {
        object[el.toUpperCase()] = true;
        arrayN.push(el);
      }
    });
  }

  return arrayN;
}

function flatArray(array) {
  return array.reduce(function (flat, toFlatten) {
    return flat.concat(
      Array.isArray(toFlatten) ? flatArray(toFlatten) : toFlatten
    );
  }, []);
}

function reverse(array) {
  var ArrayReversed = new Array();
  for (var i = array.length - 1; i >= 0; i--) {
    ArrayReversed.push(array[i]);
  }
  return ArrayReversed;
}

function orderBy(parametro) {}
