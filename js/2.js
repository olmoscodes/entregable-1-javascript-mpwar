/**
 * Modifica las funciones para implementar la funcionalidad especificada.
 * Puedes añadir funciones adicionales si lo necesitas.
 * Este archivo debe ser ejecutable (no incluyas código inválido
 * o no relacionado con el ejercicio).
 */

function getVideosInfo(videos) {
  let info = { categories: null, top_video: null, duration_average: null };

  const getCategories = videos.reduce(function (array, value) {
    if (!array[value.category]) {
      array[value.category] = "";
    }
    const totalCategory = videos.filter((element) => {
      return element.category === value.category;
    }).length;

    array[value.category] = totalCategory;
    return array;
  }, {});

  let top_video;
  let viewest = 0;
  let likest = 0;

  for (var element of videos) {
    if (element.likes > likest && element.views > viewest) {
      viewest = element.views;
      likest = element.views;
      top_video = element.title;
    }
  }

  let durationTotal = 0;
  let average = 0;

  for (let i of videos) {
    let duration = parseInt(i.duration.substring(0, 1));
    durationTotal = durationTotal + duration;
  }

  let videosQuantity = videos.length;
  average = durationTotal / videosQuantity;

  info.categories = getCategories;
  info.top_video = top_video;
  info.duration_average = `${average}min`;

  return info;
}

function getCategoryDetails(videos, category) {
  let info = {
    videos: null,
    best_video: null,
    total_views: null,
    duration_average: null,
  };

  let totalCategory = 0;
  let arrayCategories = [];

  videos.reduce(function (array, value) {
    if (value.category === category) {
      totalCategory = videos.filter((element) => {
        return element.category === value.category;
      }).length;

      info.videos = totalCategory;

      arrayCategories.push(value);
    }

    return array;
  });

  let top_video;
  let likest = 0;
  let views = 0;
  let duration = 0;

  for (var element of arrayCategories) {
    if (element.likes > likest) {
      likest = element.views;
      top_video = element.id;
    }
    views += element.views;

    duration += parseInt(element.duration);
  }

  info.best_video = top_video;
  info.total_views = views;

  let average_result = duration / totalCategory;

  info.duration_average = parseFloat(average_result).toFixed(2);

  return info;
}
