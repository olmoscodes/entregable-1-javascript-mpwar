/**
 * Modifica las funciones para implementar la funcionalidad especificada.
 * Puedes añadir funciones adicionales si lo necesitas.
 * Este archivo debe ser ejecutable (no incluyas código inválido
 * o no relacionado con el ejercicio).
 */

function checkSyntax(string) {

  let charactersToChange = "[]{}()<>";
  let totalResult= [];
  const checkParam = typeof string;


  if (checkParam !== "string") {
    return console.error("Introduce un string, por favor.");
  } else {
    for (let elem of string) {
      let eachElem = charactersToChange.indexOf(elem);
      if (eachElem >= 0) {
        if (eachElem % 2 === 0) {
          totalResult.push(eachElem + 1);
        } else {
          if (totalResult.pop() !== eachElem) {
            return false;
          }
        }
      }
    }
  }

  return totalResult.length === 0;
}
